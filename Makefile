# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: abary <marvin@42.fr>                       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/11/23 12:31:57 by abary             #+#    #+#              #
#    Updated: 2016/03/12 16:44:52 by abary            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = filler

INC_DIR = includes

LIB_DIR = libft

INC_LIB_DIR = $(LIB_DIR)/$(INC_DIR)

NAME_LIB = filler.a\

CFLAGS = -Wall -Werror -Wextra -I$(INC_DIR) -I $(INC_LIB_DIR)
#CFLAGS =-I$(INC_DIR) -I $(INC_LIB_DIR)

SRC = main.c ft_get_map.c ft_display_error.c ft_get_player.c\
	  ft_filler.c ft_recup_input.c ft_get_piece.c ft_ia.c ft_decalage_piece.c\
	  ft_free_piece_map.c ft_debug.c ft_check_pos.c ft_where.c\
	  ft_calcul_decalage.c ft_calcul_poids.c ft_parcours.c ft_pos_piece.c\
	  ft_decal.c ft_parcours_map.c ft_calcul_parcours.c ft_del_end.c

SRCS = $(addprefix sources/,$(SRC))

OBJ = $(SRCS:.c=.o)
CC = gcc
all : $(NAME)

$(NAME) : $(OBJ)
	(cd $(LIB_DIR) && $(MAKE))
	ar -r $(NAME_LIB) $(OBJ)
	gcc -o $(NAME) filler.a $(NAME_LIB) libft/libft.a

clean :
	(cd $(LIB_DIR) && make clean && cd ..)
	rm -rf $(OBJ)

fclean : clean
	(cd $(LIB_DIR) && make fclean && cd ..)
	rm -rf $(NAME)
	rm -rf $(NAME_LIB)

re : fclean all

.PHONY: all clean flcean re
