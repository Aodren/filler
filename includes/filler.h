/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/25 20:47:13 by abary             #+#    #+#             */
/*   Updated: 2016/03/12 17:23:45 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLER_H
# define FILLER_H
# define PLAYER 0
# define LIGNE 1
# define COL 2
# define I 0
# define J 1
# define XMAX 2
# define YMAX 3
# define DROITE 0
# define GAUCHE 1
# define BAS 2
# define HAUT 3
# define PLAY 4
# define WHERE 5
# define C 6
# define PX 2
# define PY 3
# define CY 7
# define CX 8
# define POIDS 0
# define RET 1
# define CHECK 2
# define OK 3

void			db_tab(char **tab);

typedef struct	s_mlx
{
	void	*mlx;
	void	*win;
	char	**map;
	char	**piece;
	int		tab[3];
}				t_mlx;
/*
*******************************************************************************
**									Recup data								  *
*******************************************************************************
*/

char			**ft_recup_input(int *tab);
int				ft_getnumber_of_player(char *line);
int				ft_get_map(char *ligne, int *tab);
int				ft_free_get_player(char *line, int ret);
int				ft_get_first_line(char *line);
char			**ft_get_game(char **map, int *tab, char *input);
char			**ft_get_piece(void);
char			**ft_recup_game(int *tab);
char			**ft_free_tab(char **tab);

/*
*******************************************************************************
**							Error management								  *
*******************************************************************************
*/

void			ft_error_player(int player);

/*
*******************************************************************************
**							     GAME										  *
*******************************************************************************
*/
void			ft_filler(int *tab, char **map, char **piece);

/*
*******************************************************************************
**							     AI      									  *
*******************************************************************************
*/

int				ft_ia(int *tab, char **map, char **piece, char c);
/*
** Decalage piece
*/
char			**ft_decal_haut(char **piece);
char			**ft_decal_bas(char **piece);
char			**ft_decal_droite(char **piece);
char			**ft_decal_gauche(char **piece);
char			**ft_del_end(char **piece);
int				ft_calc_decal_haut(char **piece);
int				ft_calc_decal_bas(char **piece, int i);
int				ft_calc_decal_droite(char **piece);
int				ft_calc_decal_gauche(char **piece);
char			**ft_decal(int *decal, char **piece);
int				ft_decalage_piece_gauche(char **piece);
int				ft_decalage_piece_droite(char **piece);
/*
** check pos piece
*/
int				ft_check_pos_piece(int *cor, char **map,
		char **piece, int *pos);
/*
** check position ennemi
*/
void			ft_where_filler_adv(char play, char **map, int *ligne,
		int *col);
int				ft_where_i_play(int mey, int mex, int advx, int advy);
int				ft_where_x_piece(char **map, char **piece, int *cor, int *pos);
/*
**	parcours
*/
int				ft_parcours_bas_droite(char **map, int *pos, char **piece,
		int *d);
int				ft_parcours_bas_gauche(char **map, char **piece, int *pos,
		int *d);
int				ft_parcours_haut_droite(char **map, char **piece, int *pos,
		int *d);
int				ft_parcours_haut_gauche(char **map, char **piece, int *pos,
		int *d);
int				ft_calcul_poids_droite(char **map, int taillex, char play);
int				ft_calcul_poids_gauche(char **map, int taillex, char play);
int				ft_calcul_poids_haut(char **map, int tailley, char play);
int				ft_calcul_poids_bas(char **map, int tailley, char play);
int				ft_calc_where(char **map, char play);
void			ft_calcul_bas_droite(char **map, int *pos, int *poid, int *cor);
void			ft_calcul_bas_gauche(char **map, int *pos, int *poid, int *cor);
void			ft_calcul_haut_droite(char **map, int *pos, int *poid,
		int *cor);
void			ft_calcul_haut_gauche(char **map, int *pos, int *poid,
		int *cor);
/*
** pos piece
*/
void			ft_pos_piece(int *cor, int *pos, char **map, char **piece);
void			ft_clear_piece(int *cor, int *pos, char **map,
		char **piece);
#endif
