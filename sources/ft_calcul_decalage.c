/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calcul_decalage.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/07 15:48:18 by abary             #+#    #+#             */
/*   Updated: 2016/03/12 17:01:11 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_calc_decal_bas(char **piece, int i)
{
	int j;

	while (i > 0)
	{
		j = 0;
		while (piece[i][j] == '.')
			++j;
		if (piece[i][j])
			break ;
		--i;
	}
	return (i);
}

int		ft_calc_decal_droite(char **piece)
{
	int	i;
	int	j;
	int	xmax;
	int	ret;

	xmax = 0;
	while (piece[0][xmax])
		++xmax;
	--xmax;
	ret = xmax;
	i = 0;
	while (piece[i])
	{
		j = xmax;
		while (piece[i][j] == '.' && j > 0)
			--j;
		if (j == xmax)
			return (0);
		else if (ret > xmax - j)
			ret = xmax - j;
		++i;
	}
	return (ret);
}

int		ft_calc_decal_gauche(char **piece)
{
	int y;
	int x;
	int tmp;
	int taillex;

	taillex = 100;
	y = 0;
	while (piece[y])
	{
		x = 0;
		tmp = 0;
		while (piece[y][x] == '.')
			++x;
		if (taillex > x)
			taillex = x;
		++y;
	}
	if (taillex == 100)
		taillex = 0;
	return (taillex);
}

int		ft_decalage_piece_droite(char **piece)
{
	int	i;
	int	j;
	int	xmax;
	int	ret;

	xmax = 0;
	while (piece[0][xmax])
		++xmax;
	--xmax;
	ret = xmax;
	i = 0;
	while (piece[i])
	{
		j = xmax;
		while (piece[i][j] == '.' && j > 0)
			--j;
		if (j == xmax)
			return (0);
		else
			ret = xmax - j;
		if (piece[i][j])
			break ;
		++i;
	}
	return (ret);
}

int		ft_decalage_piece_gauche(char **piece)
{
	int y;
	int x;
	int tmp;
	int taillex;

	taillex = 100;
	y = 0;
	while (piece[y])
	{
		x = 0;
		tmp = 0;
		while (piece[y][x] == '.')
			++x;
		if (taillex > x)
			taillex = x;
		if (piece[y][x])
			break ;
		++y;
	}
	if (taillex == 100)
		taillex = 0;
	return (taillex);
}
