/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_error.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/26 16:36:55 by abary             #+#    #+#             */
/*   Updated: 2016/03/02 13:02:19 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_error_player(int player)
{
	if (player == -3)
		ft_printf_error("Wrong number of player\n");
	else if (player == -2)
		ft_printf_error("Wrong piece\n");
	else
		ft_printf_error("Wrong map\n");
}
