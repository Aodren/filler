/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_decal.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/12 12:49:55 by abary             #+#    #+#             */
/*   Updated: 2016/03/12 17:01:26 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

char	**ft_decal(int *decal, char **piece)
{
	int i;

	i = 0;
	decal[DROITE] = ft_decalage_piece_droite(piece);
	decal[BAS] = ft_calc_decal_bas(piece, i);
	decal[BAS] = i - decal[BAS];
	decal[HAUT] = ft_calc_decal_haut(piece);
	decal[GAUCHE] = ft_calc_decal_gauche(piece);
	piece = ft_decal_haut(piece);
	piece = ft_decal_gauche(piece);
	piece = ft_del_end(piece);
	return (piece);
}
