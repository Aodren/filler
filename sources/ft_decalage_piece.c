/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_decalage_piece.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/03 15:51:31 by abary             #+#    #+#             */
/*   Updated: 2016/03/12 17:07:48 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "filler.h"

char	**ft_decal_haut(char **piece)
{
	int i;
	int j;
	int decal;

	i = ft_calc_decal_haut(piece);
	decal = 0;
	while (piece[decal + i])
	{
		piece[decal] = ft_strcpy(piece[decal], piece[decal + i]);
		++decal;
	}
	while (piece[decal])
	{
		j = 0;
		while (piece[decal][j])
			piece[decal][j++] = '.';
		++decal;
	}
	return (piece);
}

char	**ft_decal_bas(char **piece)
{
	int i;
	int j;
	int decal;

	i = 0;
	while (piece[i])
		++i;
	--i;
	decal = i;
	i = ft_calc_decal_bas(piece, i);
	while (i >= 0)
	{
		piece[decal] = ft_strcpy(piece[decal], piece[i]);
		--decal;
		--i;
	}
	while (decal >= 0)
	{
		j = 0;
		while (piece[decal][j])
			piece[decal][j++] = '.';
		--decal;
	}
	return (piece);
}

char	**ft_decal_droite(char **piece)
{
	int		i;
	int		size;
	int		decal;
	char	*d;

	i = 0;
	i = ft_calc_decal_droite(piece);
	decal = 0;
	size = 0;
	while (piece[0][size])
		++size;
	size = size - i;
	while (piece[decal])
	{
		d = piece[decal];
		piece[decal] = ft_memmove(piece[decal] + i, piece[decal], size);
		piece[decal] = d;
		piece[decal] = ft_memset(piece[decal], '\0', i);
		++decal;
	}
	return (piece);
}

char	**ft_decal_gauche(char **piece)
{
	int		i;
	int		size;
	int		decal;
	char	*d;

	i = 0;
	i = ft_calc_decal_gauche(piece);
	decal = 0;
	size = 0;
	while (piece[0][size])
		++size;
	size = size - i;
	while (piece[decal])
	{
		d = piece[decal];
		piece[decal] = ft_memmove(piece[decal], piece[decal] + i, size);
		piece[decal] = d;
		d = piece[decal];
		piece[decal] = ft_memset(piece[decal] + size, '\0', i);
		piece[decal] = d;
		++decal;
	}
	return (piece);
}
