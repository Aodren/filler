/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recup_input.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/02 15:50:56 by abary             #+#    #+#             */
/*   Updated: 2016/03/12 17:09:55 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "filler.h"

static void		ft_init_tab(int *tab)
{
	tab[PLAYER] = 0;
	tab[COL] = 0;
	tab[LIGNE] = 0;
}

char			**ft_recup_input(int *tab)
{
	char	**map;
	char	*line;
	int		i;

	map = NULL;
	line = NULL;
	i = 0;
	ft_init_tab(tab);
	while (get_next_line(0, &line) == 1)
	{
		if (i == 0 && (tab[PLAYER] = ft_getnumber_of_player(line)) < 1)
			break ;
		else if (i == 1 && ((ft_get_map(line, tab) < 1)))
			break ;
		else if (i == 2 && (ft_get_first_line(line) < 1))
			break ;
		else if (i >= tab[LIGNE] + 2 && (map = ft_get_game(map, tab, line)))
			break ;
		else if (i > 2 && !((map = ft_get_game(map, tab, line))))
			break ;
		++i;
	}
	return (map);
}

char			**ft_recup_game(int *tab)
{
	char	**map;
	char	*line;
	int		i;

	i = 0;
	map = NULL;
	while (get_next_line(0, &line) == 1)
	{
		if (i > 1 && !(map = ft_get_game(map, tab, line)))
			break ;
		if (i > tab[LIGNE])
			break ;
		++i;
	}
	return (map);
}
