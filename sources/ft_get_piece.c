/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_piece.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/02 13:07:26 by abary             #+#    #+#             */
/*   Updated: 2016/03/12 16:46:54 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "filler.h"
#include <stdlib.h>

static char		**ft_return_piece(char *input, char **piece)
{
	free(input);
	return (piece);
}

static char		**ft_malloc_piece(char **piece, int line, int col)
{
	int	i;

	col = 0;
	i = 0;
	if (!(piece = (char **)ft_memalloc(sizeof(char *) * (line + 1))))
		return (NULL);
	return (piece);
}

static char		**ft_init_piece(char **piece, int *line)
{
	int		col;
	char	*input;
	char	*str;

	input = NULL;
	if (get_next_line(0, &input) < 0)
		return (NULL);
	if (!(str = ft_strstr(input, "Piece ")))
		return (ft_return_piece(input, piece));
	while (!ft_isdigit(*str) && *input)
		++str;
	if (*str)
		*line = ft_atoi(str);
	else
		return (ft_return_piece(input, piece));
	while (*str != ' ')
		++str;
	if (*str && line)
		col = ft_atoi(str);
	else
		return (ft_return_piece(input, piece));
	if (!col)
		return (ft_return_piece(input, piece));
	return (ft_malloc_piece(piece, *line, col));
}

static char		**ft_fill_piece(char **piece, int line)
{
	char	*input;
	int		i;

	input = NULL;
	i = 0;
	while (i < line)
	{
		get_next_line(0, &input);
		piece[i] = ft_strdup(input);
		free(input);
		++i;
	}
	return (piece);
}

char			**ft_get_piece(void)
{
	char	**piece;
	int		line;

	piece = NULL;
	piece = ft_init_piece(piece, &line);
	if (piece)
		piece = ft_fill_piece(piece, line);
	return (piece);
}
