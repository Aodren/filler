/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_where.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 12:54:33 by abary             #+#    #+#             */
/*   Updated: 2016/03/12 17:13:50 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void		ft_where_filler_adv(char play, char **map, int *ligne, int *col)
{
	int i;
	int j;
	int	find;

	find = 1;
	if (play == 'o')
		play = 'x';
	else
		play = 'o';
	i = 0;
	while (map[i] && find)
	{
		j = 0;
		while (map[i][j] && find)
		{
			if (map[i][j] == play || map[i][j] == (play - 32))
			{
				*ligne = i;
				*col = j;
				find = 0;
			}
			++j;
		}
		++i;
	}
}

/*
** Determine ou on dois joeur
** ret = 1x jouer en bas
** ret = 2x jouer en haut
** ret = x1 joueur a droite
** ret = x2 jouer a gauche
*/

int			ft_where_i_play(int mey, int mex, int advx, int advy)
{
	int ret;

	ret = 0;
	if (mey < advy)
		ret = 10;
	else
		ret = 20;
	if (mex < advx)
		ret += 1;
	else
		ret += 2;
	return (ret);
}

static void	ft_init_poid(int *poid, int *pos)
{
	poid[POIDS] = -100;
	pos[CY] = -1;
	poid[CHECK] = 0;
	poid[RET] = 0;
	poid[OK] = 0;
}

int			ft_where_x_piece(char **map, char **piece, int *cor, int *pos)
{
	int poid[4];

	ft_init_poid(poid, pos);
	while (piece[++pos[CY]])
	{
		pos[CX] = -1;
		while (piece[pos[CY]][++pos[CX]])
		{
			poid[CHECK] = ft_check_pos_piece(cor, map, piece, pos);
			if (poid[CHECK] && (poid[OK] = 1))
			{
				ft_pos_piece(cor, pos, map, piece);
				if (pos[WHERE] == 11)
					ft_calcul_bas_droite(map, pos, poid, cor);
				else if (pos[WHERE] == 12)
					ft_calcul_bas_gauche(map, pos, poid, cor);
				else if (pos[WHERE] == 21)
					ft_calcul_haut_droite(map, pos, poid, cor);
				else if (pos[WHERE] == 22)
					ft_calcul_haut_gauche(map, pos, poid, cor);
				ft_clear_piece(cor, pos, map, piece);
			}
		}
	}
	return (poid[OK]);
}
