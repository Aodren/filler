/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/25 20:43:48 by abary             #+#    #+#             */
/*   Updated: 2016/03/12 17:10:23 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"
#include <stdlib.h>

int		main(void)
{
	char	**map;
	char	**piece;
	int		tab[3];

	map = ft_recup_input(tab);
	if ((piece = ft_get_piece()))
		ft_filler(tab, map, piece);
	return (0);
}
