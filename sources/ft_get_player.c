/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_player.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/26 14:44:25 by abary             #+#    #+#             */
/*   Updated: 2016/03/04 16:08:20 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "filler.h"
#include <stdlib.h>

int			ft_free_get_player(char *line, int ret)
{
	free(line);
	line = NULL;
	if (ret < 1)
		ft_error_player(ret);
	return (ret);
}

/*
** Joueur 1 = 1
** Joueur 2 = 2
** IA adverse : +10
** IA joueur : +20
*/

static	int	ft_return_player(char *line, int player)
{
	char *str;

	if ((str = ft_strstr(line, " : [./filler]")))
		return (ft_free_get_player(line, player + 20));
	if ((str = ft_strstr(line, " : [")))
		if ((str = ft_strstr(line, "]")))
			return (ft_free_get_player(line, player + 10));
	return (ft_free_get_player(line, -3));
}

int			ft_getnumber_of_player(char *line)
{
	int		player;
	char	*str;

	player = 0;
	if ((str = ft_strstr(line, "$$$ exec p")))
	{
		while (*str != 'p')
			++str;
		player = ft_atoi(++str);
		if (player != 1 && player != 2)
			return (ft_free_get_player(line, -3));
	}
	else
		return (ft_free_get_player(line, -2));
	return (ft_return_player(line, player));
}
