/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parcours_map.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/12 12:55:54 by abary             #+#    #+#             */
/*   Updated: 2016/03/12 17:09:17 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		ft_calc_where(char **map, char play)
{
	int where;
	int tmp[2];
	int find;
	int ligne;
	int col;

	find = 1;
	tmp[0] = 0;
	ft_where_filler_adv(play, map, &ligne, &col);
	while (map[tmp[0]] && find)
	{
		tmp[1] = 0;
		while (map[tmp[0]][tmp[1]] && find)
		{
			if (map[tmp[0]][tmp[1]] == play ||
					map[tmp[0]][tmp[1]] == (play - 32))
				find = 0;
			++tmp[1];
		}
		++tmp[0];
	}
	where = ft_where_i_play(tmp[0], tmp[1], col, ligne);
	return (where);
}
