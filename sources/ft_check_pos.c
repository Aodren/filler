/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_pos.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/04 19:21:57 by abary             #+#    #+#             */
/*   Updated: 2016/03/12 17:05:34 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

/*
** decalage en haut a gauche
*/

static int	ft_check_line(char *map, char *piece, int xmap, int *pos)
{
	int i;

	i = 0;
	while (piece[i + pos[CX]])
	{
		if (i + xmap >= pos[XMAX])
			return (0);
		if (map[i + xmap] != '.' && piece[i + pos[CX]] == '*')
			return (0);
		++i;
	}
	i = 0;
	while (pos[CX] - i >= 0)
	{
		if (xmap - i < 0)
			return (0);
		if (map[xmap - i] != '.' && piece[pos[CX] - i] == '*')
			return (0);
		++i;
	}
	return (1);
}

static int	ft_check_haut(char **map, int *pos, int *cor, char **piece)
{
	int i;
	int ret;

	ret = 0;
	i = 1;
	while (pos[CY] - i >= 0)
	{
		if (piece[pos[CY] - i])
		{
			if (cor[I] - i < 0)
				return (0);
			ret = ft_check_line(map[cor[I] - i],
					piece[pos[CY] - i], cor[J], pos);
			if (!ret)
				return (0);
		}
		++i;
	}
	return (1);
}

int			ft_check_pos_piece(int *cor, char **map, char **piece, int *pos)
{
	int i;
	int ret;
	int nbr;

	nbr = 0;
	i = 0;
	ret = 0;
	if (piece[pos[CY]][pos[CX]] == '.')
		return (0);
	i = 0;
	while (piece[pos[CY] + i])
	{
		if (piece[pos[CY] + i][0])
		{
			if (cor[I] + i >= pos[YMAX])
				return (0);
			if (!(ret = ft_check_line(map[cor[I] + i],
							piece[pos[CY] + i], cor[J], pos)))
				return (0);
		}
		++i;
	}
	return (ft_check_haut(map, pos, cor, piece));
}
