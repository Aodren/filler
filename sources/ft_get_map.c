/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_map.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/29 20:04:32 by abary             #+#    #+#             */
/*   Updated: 2016/03/12 12:30:33 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "filler.h"
#include <stdlib.h>
#include <unistd.h>

int				ft_get_map(char *ligne, int *tab)
{
	char	*str;

	if ((str = ft_strstr(ligne, "Plateau ")))
	{
		while (!(ft_isdigit(*str) && *str))
			++str;
		if (!*str)
			return (ft_free_get_player(ligne, 0));
		tab[LIGNE] = ft_atoi(str);
		while ((ft_isdigit(*str) && *str))
			++str;
		if (!*str || *str != ' ')
			return (ft_free_get_player(ligne, 0));
		tab[COL] = ft_atoi(str);
	}
	else
		return (ft_free_get_player(ligne, 0));
	free(ligne);
	return (1);
}

int				ft_get_first_line(char *line)
{
	char	nbr;
	char	*d;

	nbr = 48;
	d = line;
	while (*line == ' ')
		++line;
	while (*line)
	{
		if (*line != nbr)
			return (ft_free_get_player(d, 0));
		++line;
		++nbr;
		if (nbr == 58)
			nbr = 48;
	}
	free(d);
	return (1);
}

static	char	**ft_init_map(int line, int col)
{
	char	**map;
	int		len;
	int		i;

	i = 0;
	len = (col + 5);
	if (!(map = ft_memalloc(sizeof(char *) * (line + 1))))
		return (NULL);
	return (map);
}

char			**ft_get_game(char **map, int *tab, char *input)
{
	int	i;

	i = 0;
	if (!map)
		map = ft_init_map(tab[LIGNE], tab[COL]);
	while (i <= tab[LIGNE])
	{
		if (!*(map + i))
		{
			*(map + i) = ft_strdup(input + 4);
			break ;
		}
		++i;
	}
	free(input);
	input = NULL;
	return (map);
}
