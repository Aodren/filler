/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calcul_poids.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/07 21:35:22 by abary             #+#    #+#             */
/*   Updated: 2016/03/12 17:03:16 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_calcul_poids_droite(char **map, int taillex, char play)
{
	int	poids;
	int i;
	int j;

	i = 0;
	poids = 1;
	taillex /= 2;
	while (map[i])
	{
		j = 0;
		while (map[i][taillex + j])
		{
			if (map[i][taillex + j] == 'A' || map[i][taillex + j] == play
					|| map[i][taillex + j] == (play - 32))
				++poids;
			++j;
		}
		++i;
	}
	return (poids);
}

int		ft_calcul_poids_gauche(char **map, int taillex, char play)
{
	int	poids;
	int i;
	int j;

	i = 0;
	poids = 1;
	taillex /= 2;
	while (map[i])
	{
		j = 0;
		while (j < taillex)
		{
			if (map[i][j] == 'A' || map[i][j] == play
					|| map[i][j] == (play - 32))
				++poids;
			++j;
		}
		++i;
	}
	return (poids);
}

int		ft_calcul_poids_haut(char **map, int tailley, char play)
{
	int	poids;
	int i;
	int j;

	i = 0;
	poids = 1;
	tailley /= 2;
	while (i < tailley)
	{
		j = 0;
		while (map[i][j])
		{
			if (map[i][j] == 'A' || map[i][j] == (play - 32)
					|| map[i][j] == play)
				++poids;
			++j;
		}
		++i;
	}
	return (poids);
}

int		ft_calcul_poids_bas(char **map, int tailley, char play)
{
	int	poids;
	int i;
	int j;

	i = 0;
	poids = 1;
	tailley /= 2;
	while (map[i + tailley])
	{
		j = 0;
		while (map[i + tailley][j])
		{
			if (map[i + tailley][j] == 'A' || map[i + tailley][j] == play
					|| map[i + tailley][j] == (play - 32))
				++poids;
			++j;
		}
		++i;
	}
	return (poids);
}
