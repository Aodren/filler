/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ia.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/02 20:00:30 by abary             #+#    #+#             */
/*   Updated: 2016/03/12 17:08:48 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "filler.h"

/*
** ordre "X Y\n"
*/

int	ft_ia(int *tab, char **map, char **piece, char play)
{
	int pos[9];
	int	decal[4];

	pos[YMAX] = tab[LIGNE];
	pos[XMAX] = tab[COL];
	tab[PLAYER] = tab[PLAYER] % 10;
	pos[PLAY] = play;
	piece = ft_decal(decal, piece);
	pos[WHERE] = ft_calc_where(map, play);
	if (pos[WHERE] == 11)
		return (ft_parcours_bas_droite(map, pos, piece, decal));
	else if (pos[WHERE] == 12)
		return (ft_parcours_bas_gauche(map, piece, pos, decal));
	else if (pos[WHERE] == 21)
		return (ft_parcours_haut_droite(map, piece, pos, decal));
	else if (pos[WHERE] == 22)
		return (ft_parcours_haut_gauche(map, piece, pos, decal));
	return (0);
}
