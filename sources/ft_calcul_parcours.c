/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calcul_parcours.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/12 16:03:53 by abary             #+#    #+#             */
/*   Updated: 2016/03/12 17:02:12 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void	ft_calcul_bas_droite(char **map, int *pos, int *poid, int *cor)
{
	poid[RET] = ft_calcul_poids_bas(map, pos[YMAX], pos[C]);
	poid[RET] += ft_calcul_poids_droite(map, pos[XMAX], pos[C]);
	if (poid[RET] > poid[POIDS])
	{
		cor[PX] = pos[CX];
		cor[PY] = pos[CY];
		poid[POIDS] = poid[RET];
	}
}

void	ft_calcul_bas_gauche(char **map, int *pos, int *poid, int *cor)
{
	poid[RET] = ft_calcul_poids_bas(map, pos[YMAX], pos[C]);
	poid[RET] += ft_calcul_poids_gauche(map, pos[XMAX], pos[C]);
	if (poid[RET] > poid[POIDS])
	{
		cor[PX] = pos[CX];
		cor[PY] = pos[CY];
		poid[POIDS] = poid[RET];
	}
}

void	ft_calcul_haut_droite(char **map, int *pos, int *poid, int *cor)
{
	poid[RET] = ft_calcul_poids_haut(map, pos[YMAX], pos[C]);
	poid[RET] += ft_calcul_poids_droite(map, pos[XMAX], pos[C]);
	if (poid[RET] > poid[POIDS])
	{
		cor[PX] = pos[CX];
		cor[PY] = pos[CY];
		poid[POIDS] = poid[RET];
	}
}

void	ft_calcul_haut_gauche(char **map, int *pos, int *poid, int *cor)
{
	poid[RET] = ft_calcul_poids_haut(map, pos[YMAX], pos[C]);
	poid[RET] += ft_calcul_poids_gauche(map, pos[XMAX], pos[C]);
	if (poid[RET] > poid[POIDS])
	{
		cor[PX] = pos[CX];
		cor[PY] = pos[CY];
		poid[POIDS] = poid[RET];
	}
}
