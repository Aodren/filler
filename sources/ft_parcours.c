/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parcours.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/07 21:45:52 by abary             #+#    #+#             */
/*   Updated: 2016/03/12 17:19:04 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"
#include "libft.h"

static int	ft_return(int y, int x)
{
	ft_putnbr(y);
	ft_putchar(' ');
	ft_putnbr(x);
	ft_putchar('\n');
	return (1);
}

int			ft_parcours_bas_droite(char **map, int *pos, char **piece, int *d)
{
	int		cor[4];

	cor[I] = pos[YMAX];
	cor[PX] = 0;
	cor[PY] = 0;
	while (--cor[I] >= 0)
	{
		cor[J] = pos[XMAX];
		while (--cor[J] >= 0)
		{
			if (map[cor[I]][cor[J]] == pos[PLAY]
					|| map[cor[I]][cor[J]] == (pos[PLAY] - 32))
			{
				pos[C] = map[cor[I]][cor[J]];
				map[cor[I]][cor[J]] = '.';
				if (ft_where_x_piece(map, piece, cor, pos))
				{
					return (ft_return(cor[I] - d[HAUT] - cor[PY],
								cor[J] - d[GAUCHE] - cor[PX]));
				}
				map[cor[I]][cor[J]] = pos[C];
			}
		}
	}
	return (0);
}

int			ft_parcours_bas_gauche(char **map, char **piece, int *pos, int *d)
{
	int		cor[4];

	cor[PX] = 0;
	cor[PY] = 0;
	cor[I] = pos[YMAX];
	while (--cor[I] >= 0)
	{
		cor[J] = -1;
		while (map[cor[I]][++cor[J]])
		{
			if (map[cor[I]][cor[J]] == pos[PLAY]
					|| map[cor[I]][cor[J]] == (pos[PLAY] - 32))
			{
				pos[C] = map[cor[I]][cor[J]];
				map[cor[I]][cor[J]] = '.';
				if (ft_where_x_piece(map, piece, cor, pos))
				{
					return (ft_return(cor[I] - d[HAUT] - cor[PY],
								cor[J] - d[GAUCHE] - cor[PX]));
				}
				map[cor[I]][cor[J]] = pos[C];
			}
		}
	}
	return (0);
}

int			ft_parcours_haut_droite(char **map, char **piece, int *pos, int *d)
{
	int		cor[4];

	cor[I] = -1;
	cor[PX] = 0;
	cor[PY] = 0;
	while (map[++cor[I]])
	{
		cor[J] = pos[XMAX];
		while (--cor[J] >= 0)
		{
			if (map[cor[I]][cor[J]] == pos[PLAY]
					|| map[cor[I]][cor[J]] == (pos[PLAY] - 32))
			{
				pos[C] = map[cor[I]][cor[J]];
				map[cor[I]][cor[J]] = '.';
				{
					if (ft_where_x_piece(map, piece, cor, pos))
						return (ft_return(cor[I] - d[HAUT] - cor[PY],
									cor[J] - d[GAUCHE] - cor[PX]));
				}
				map[cor[I]][cor[J]] = pos[C];
			}
		}
	}
	return (0);
}

int			ft_parcours_haut_gauche(char **map, char **piece, int *pos, int *d)
{
	int		cor[4];

	cor[PX] = 0;
	cor[PY] = 0;
	cor[I] = -1;
	while (map[++cor[I]])
	{
		cor[J] = -1;
		while (map[cor[I]][++cor[J]])
		{
			if (map[cor[I]][cor[J]] == pos[PLAY]
					|| map[cor[I]][cor[J]] == (pos[PLAY] - 32))
			{
				pos[C] = map[cor[I]][cor[J]];
				map[cor[I]][cor[J]] = '.';
				if (ft_where_x_piece(map, piece, cor, pos))
				{
					return (ft_return(cor[I] - d[HAUT] - cor[PY],
								cor[J] - d[GAUCHE] - cor[PX]));
				}
				map[cor[I]][cor[J]] = pos[C];
			}
		}
	}
	return (0);
}
