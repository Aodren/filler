/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_filler.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/02 12:39:32 by abary             #+#    #+#             */
/*   Updated: 2016/03/12 16:45:56 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"
#include "libft.h"

void		ft_filler(int *tab, char **map, char **piece)
{
	char play;

	if (tab[PLAYER] % 10 == 1)
		play = 'o';
	else
		play = 'x';
	while (1)
	{
		if (!ft_ia(tab, map, piece, play))
		{
			ft_putendl("FIN, FIN");
			break ;
		}
		map = ft_free_tab(map);
		map = ft_recup_game(tab);
		if (!map)
			break ;
		piece = ft_free_tab(piece);
		piece = NULL;
		piece = ft_get_piece();
		if (!piece)
			break ;
	}
}
