/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pos_piece.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/07 20:49:50 by abary             #+#    #+#             */
/*   Updated: 2016/03/12 17:12:28 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

static void	ft_pos_line(char *map, char *piece, int xmap, int xpiece)
{
	int i;

	i = 0;
	while (piece[i + xpiece])
	{
		if (piece[i + xpiece] == '*')
			map[i + xmap] = 'A';
		++i;
	}
	i = 0;
	while (xpiece - i >= 0)
	{
		if (piece[xpiece - i] == '*')
			map[xmap - i] = 'A';
		++i;
	}
}

void		ft_pos_piece(int *cor, int *pos, char **map, char **piece)
{
	int i;
	int ret;
	int nbr;

	nbr = 0;
	i = 0;
	ret = 0;
	i = 0;
	while (piece[pos[CY] + i])
	{
		if (piece[pos[CY] + i][0])
			ft_pos_line(map[cor[I] + i], piece[pos[CY] + i], cor[J], pos[CX]);
		++i;
	}
	i = 1;
	while (pos[CY] - i >= 0)
	{
		if (piece[pos[CY] - i])
			ft_pos_line(map[cor[I] - i], piece[pos[CY] - i], cor[J], pos[CX]);
		++i;
	}
}

static void	ft_clear_line(char *map, char *piece, int xmap, int xpiece)
{
	int i;

	i = 0;
	while (piece[i + xpiece])
	{
		if (piece[i + xpiece] == '*')
			map[i + xmap] = '.';
		++i;
	}
	i = 0;
	while (xpiece - i >= 0)
	{
		if (piece[xpiece - i] == '*')
			map[xmap - i] = '.';
		++i;
	}
}

void		ft_clear_piece(int *cor, int *pos, char **map, char **piece)
{
	int i;
	int ret;
	int nbr;

	nbr = 0;
	i = 0;
	ret = 0;
	while (piece[pos[CY] + i])
	{
		if (piece[pos[CY] + i][0])
			ft_clear_line(map[cor[I] + i], piece[pos[CY] + i], cor[J], pos[CX]);
		++i;
	}
	i = 1;
	while (pos[CY] - i >= 0)
	{
		if (piece[pos[CY] - i])
			ft_clear_line(map[cor[I] - i], piece[pos[CY] - i], cor[J], pos[CX]);
		++i;
	}
}
