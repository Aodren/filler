/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_del_end.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/12 16:42:34 by abary             #+#    #+#             */
/*   Updated: 2016/03/12 17:06:42 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"
#include "libft.h"

static char	**ft_del_del_end(char **piece)
{
	int x;
	int i;
	int size;

	x = 0;
	i = 0;
	while (piece[0][x])
		++x;
	size = x - 1;
	while (piece[i])
	{
		size = x - 1;
		while (piece[i][size] == '.')
			piece[i][size--] = '\0';
		++i;
	}
	return (piece);
}

char		**ft_del_end(char **piece)
{
	int i;
	int bas;
	int x;

	x = 0;
	i = 0;
	bas = 0;
	while (piece[0][x])
		++x;
	while (piece[i])
		++i;
	bas = ft_calc_decal_bas(piece, --i);
	while (i > bas)
	{
		ft_memset(piece[i], '\0', x);
		--i;
	}
	return (ft_del_del_end(piece));
}

int			ft_calc_decal_haut(char **piece)
{
	int i;
	int j;

	i = 0;
	while (piece[i])
	{
		j = 0;
		while (piece[i][j] == '.')
			++j;
		if (piece[i][j])
			break ;
		++i;
	}
	return (i);
}
